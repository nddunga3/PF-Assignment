//
// Created by DucDung Nguyen on 05/04/2017.
//

#include "commonLib.h"

// Load data from the input file
// return: true if loading process is success
bool loadData(char* fName, DataIterator_t& data) {
    // TODO: write your code to read data points from file and put them in vector "data"
}

// Load parameters from the input file
// return: true if loading process is success
bool loadParams(char* fName, ProbParam_t& param) {
    ifstream inFile(fName);
    if (inFile) {
        string line;
        while (getline(inFile , line)) {
            /// On Windows, lines on file ends with \r\n. So you have to remove \r
            if (line[line.length() - 1] == '\r')
                line.erase(line.length() - 1);
            if (line.length() > 0) {
                // TODO: read data from this text (hint: using istringstream)
            }
        }
        inFile.close();
    }
    else {
        cout << "The file is not found!";
    }
}
